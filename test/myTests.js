QUnit.module("Set title", function (hooks) {
    hooks.beforeEach(function () {
        var fixture = document.getElementById('qunit-fixture');
        fixture.innerHTML = '<div id="pageTitle"></div>';
    });

    QUnit.test('Set title for John, Faiza and Stan', function (assert) {
        setTitle(['John', 'Faiza', 'Stanss']);
        var title = document.getElementById('pageTitle').innerText;
        assert.equal(title, 'John, Faiza and Stanss');
    });

    QUnit.test('Set title for John and Faiza', function (assert) {
        setTitle(['John', 'Faiza']);
        var title = document.getElementById('pageTitle').innerText;
        assert.equal(title, 'John and Faiza');
    });

    QUnit.test('Set title for John', function (assert) {
        setTitle(['John']);
        var title = document.getElementById('pageTitle').innerText;
        assert.equal(title, 'John');
    });
});